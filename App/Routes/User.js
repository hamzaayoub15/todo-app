const express = require("express")
const router = express.Router()
// const users = require("../Controllers/User")
const command = require("../Command/UserCommand/CreateUser")
const login = require("../Command/UserCommand/UserLoginCommand")
//createUser
// router.post("/users", users.create)
router.post("/users", command)
//login user
router.post("/users/login", login)

module.exports = router
