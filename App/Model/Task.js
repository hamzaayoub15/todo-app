const mongoose = require("mongoose")
const user = require("../Model/User")
const todoSchema = new mongoose.Schema(
  {
    // todoId: {
    //   type: String,
    //   required: true
    // },
    description: {
      type: String,
      trim: true,
      required: true
    },
    completed: {
      type: Boolean,
      default: false
    },
    userId: {
      type: String,
      required: true,
      ref: "user"
    }
  },

  {
    timestamps: true
  }
)

const todo = mongoose.model("todo", todoSchema)
module.exports = todo
